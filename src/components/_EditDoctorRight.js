import React from "react";
import { Row, Label, Form, FormGroup, Input, Button } from "reactstrap";
import "EditDoctorRight.css";

const EditDoctorRight = () => {
  return (
    <div className="rightSide">
      <Form>
        <div>
          <h3 className="form">Upload Photo</h3>
          <hr />
          <br />
          <div className="rcorners">
            <div className="upload">
              <img src="https://ik.imagekit.io/zvqf9loqe2/VET/Vector__1__wVzNtBzycat.svg" />
            </div>
            <div className="upload-text">
              <h5 style={{ color: "#818181" }}>Upload Image</h5>
            </div>
          </div>

          <h3 className="form">Doctor Information</h3>
          <hr />

          <Label className="form">Status</Label>
          <br />
          <Row className="radioButton">
            <FormGroup check>
              <Label style={{ marginRight: "100px" }} check>
                <Input type="radio" name="radio1" />
                Active
              </Label>
              {/* </FormGroup> */}

              {/* <FormGroup check> */}
              <Label check>
                <Input type="radio" name="radio1" />
                Offline
              </Label>
            </FormGroup>
          </Row>

          <br />

          <Row>
            <div className="d-flex">
              <FormGroup className="form">
                <Label for="exampleSelect">Jam Mulai</Label>
                <Input
                  // style={{ width: "200px" }}
                  type="select"
                  name="select"
                  id="exampleSelect"
                >
                  <option>08.00</option>
                  <option>09.00</option>
                  <option>10.00</option>
                  <option>11.00</option>
                  <option>12.00</option>
                  <option>13.00</option>
                  <option>14.00</option>
                  <option>15.00</option>
                </Input>
              </FormGroup>

              <FormGroup className="form">
                <Label for="exampleSelect">Jam Pulang</Label>
                <Input
                  // style={{ width: "200px" }}
                  type="select"
                  name="select"
                  id="exampleSelect"
                >
                  <option>11.00</option>
                  <option>12.00</option>
                  <option>13.00</option>
                  <option>14.00</option>
                  <option>15.00</option>
                  <option>16.00</option>
                  <option>17.00</option>
                  <option>18.00</option>
                  <option>19.00</option>
                  <option>20.00</option>
                  <option>21.00</option>
                </Input>
              </FormGroup>
            </div>
          </Row>

          <br />
          <br />
          <h3 className="form">Basic Information</h3>
          <hr />

          <Label className="form">Nama Lengkap</Label>
          <br />
          <div className="form1">
            <Input className="input" type="text" placeholder="Alexandria" />
          </div>

          <br />
          <Label className="form">Gender</Label>
          <br />
          <Row className="radioButton">
            <FormGroup>
              <Label style={{ marginRight: "100px" }}>
                <Input type="radio" name="radio1" />
                Male
              </Label>

              <Label>
                <Input type="radio" name="radio1" />
                Female
              </Label>
            </FormGroup>
          </Row>

          <br />

          <Label className="form">Experience</Label>
          <br />
          <div className="form1">
            <Input className="input" type="text" placeholder="12 Tahun" />
          </div>

          <br />
          <br />
          <h3 className="form">Contact Details</h3>
          <hr />

          <Label className="form">Nomor Telephone</Label>
          <br />
          <div className="form1">
            <Input
              className="input"
              type="telephone"
              placeholder="+712371723 18231823"
            />
          </div>

          <br />
          <Label className="form">Email</Label>
          <br />
          <div className="form1">
            <Input
              className="input"
              type="email"
              placeholder="alexanda@gmail.com"
            />
          </div>
          <br />
          <br />
          <br />
        </div>
        <Button className="tombol-simpan" type="simpan" color="warning">
          Simpan
        </Button>
      </Form>
    </div>
  );
};

export default EditDoctorRight;
