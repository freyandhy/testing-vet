import React, { useState, useEffect } from "react";
import {
  Container,
  Row,
  Col,
  CardBody,
  CardText,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import "./BookFromPatient.css";
import "../pages/EditUser.css";
import axios from "axios";

const BookFromPatient = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [modalAccept, setModalAccept] = useState(false);
  const [modalDecline, setModalDecline] = useState(false);
  const [bookFromPatients, setBookFromPatients] = useState([]);
  const [patientId, setPatientId] = useState(0);

  useEffect(() => {
    setIsLoading(true);
    getData();
    setIsLoading(false);
  }, []);

  const getData = () => {
    const url = "https://5fbcb4963f8f90001638c271.mockapi.io/bookings";
    axios.get(url).then((res) => {
      setBookFromPatients(res.data);
      // console.log(res.data);
    });
  };

  const toggleAccept = () => {
    setModalAccept(!modalAccept);
  };

  const toggleDecline = () => {
    setModalDecline(!modalDecline);
  };

  const modalAcceptClickAccept = (e, id) => {
    e.preventDefault();
    setIsLoading(true);
    getData();
    const url = "https://5fbcb4963f8f90001638c271.mockapi.io/bookings";
    axios.delete(`${url}/${id}`).then((res) => {
      console.log(res.data);
      getData();
      setIsLoading(false);
      toggleAccept();
    });
  };

  const modalAcceptClickCancel = () => {
    toggleAccept();
  };

  return (
    <>
      <Container>
        {isLoading ? (
          <div>
            <div className="spinner">
              <div className="bounce1"></div>
              <div className="bounce2"></div>
              <div className="bounce3"></div>
            </div>
          </div>
        ) : (
          bookFromPatients.map((BookFromPatientList) => {
            // console.log(BookFromPatientList);
            return (
              <div
                tabIndex="1"
                className="card w-100 doctor-approval-card mb-2"
                key={BookFromPatientList.id}
              >
                <CardBody>
                  <Row className="text-center">
                    <Col>
                      <div className="approval-date d-flex align-items-center">
                        {BookFromPatientList.tgl}
                      </div>
                    </Col>
                    <Col sm="2" className="d-flex align-items-center">
                      <div className="approval-name">
                        {BookFromPatientList.user}
                      </div>
                    </Col>
                    <Col className="d-flex align-items-center justify-content-center">
                      <div className="approval-pet-counts">
                        {BookFromPatientList.pets}
                      </div>
                    </Col>
                    <Col className="d-flex align-items-center justify-content-center">
                      <div className="approval-time">
                        {BookFromPatientList.jam}
                      </div>
                    </Col>
                    <Col sm="4">
                      <span
                        className="mr-1 btn btn-approve"
                        onClick={() => {
                          setModalAccept(!modalAccept);
                          setPatientId(BookFromPatientList.id);
                          // console.log(patientId);
                          // toggleAccept;
                        }}
                      >
                        Approve
                      </span>
                      <span
                        className="ml-1 btn btn-decline"
                        onClick={toggleDecline}
                      >
                        Decline
                      </span>
                    </Col>
                  </Row>
                </CardBody>
              </div>
            );
          })
        )}
        <Modal
          isOpen={modalAccept}
          toggle={toggleAccept}
          className="doctor-modal-placement"
          // onClick={() => console.log(BookFromPatientList.id)}
        >
          <ModalBody>Are you sure want to accept?</ModalBody>

          <ModalFooter className="justify-content-center">
            <Button
              color="light"
              onClick={modalAcceptClickCancel}
              className="px-5 mx-3"
            >
              Cancel
            </Button>
            <Button
              color="warning"
              onClick={(e) => {
                // console.log(BookFromPatientList.id);
                modalAcceptClickAccept(e, patientId);
              }}
              className="px-5 mx-3"
            >
              Accept
            </Button>
          </ModalFooter>
        </Modal>
        {/* modal decline start */}
        <Modal
          isOpen={modalDecline}
          toggle={toggleDecline}
          className="doctor-modal-placement"
        >
          <ModalBody>Are you sure want to Decline?</ModalBody>

          <ModalFooter className="justify-content-center">
            <Button color="light" onClick={toggleDecline} className="px-5 mx-3">
              Cancel
            </Button>
            <Button
              color="warning"
              onClick={toggleDecline}
              className="px-5 mx-3"
            >
              Yes
            </Button>
          </ModalFooter>
        </Modal>
      </Container>
    </>
  );
};

export default BookFromPatient;
