import React, { useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import add from './../img/Vector (1).png'
import './modalPetInfo.css'

const ModalExample = (props) => {
    const {
        buttonLabel,
        className
    } = props;

    const [modal, setModal] = useState(false);

    const [spesies, setSpesies] = useState('')
    const handlerSpesies = (e) => {
        setSpesies(e.target.value)
    }

    const [gender, setGender] = useState('')
    const handlerGender = (e) => {
        setGender(e.target.value)
    }

    const [name, setName] = useState('')
    const handlerName = e => {
        setName(e.target.value)
    }

    const history = useHistory()
    const params = useParams().id
    const handleSubmit1 = e => {
        toggle()
        localStorage.setItem('namaHewan1', name)
        localStorage.setItem('spesies1', spesies)
        localStorage.setItem('gender1', gender)
        history.push(`/book-clinic/${params}`)
    }

    const handleSubmit2 = e => {
        toggle()
        localStorage.setItem('namaHewan2', name)
        localStorage.setItem('spesies2', spesies)
        localStorage.setItem('gender2', gender)
        history.push(`/book-clinic/${params}`)
    }

    const handleSubmit3 = e => {
        toggle()
        localStorage.setItem('namaHewan3', name)
        localStorage.setItem('spesies3', spesies)
        localStorage.setItem('gender3', gender)
        history.push(`/book-clinic/${params}`)
    }

    const handleSubmit4 = e => {
        toggle()
        localStorage.setItem('namaHewan4', name)
        localStorage.setItem('spesies4', spesies)
        localStorage.setItem('gender4', gender)
        history.push(`/book-clinic/${params}`)
    }

    const toggle = () => setModal(!modal);

    return (
        <div>
            <div className="modal-pet">
                <img src={add} onClick={toggle} alt="" />
                <Modal isOpen={modal} toggle={toggle} className='modal-pet'>
                    <ModalHeader className='modal-header-pet' toggle={toggle}>
                        <h1>Informasi Hewan Peliharaan</h1>
                    </ModalHeader>
                    <ModalBody className='form-pet'>
                        <form action="yoi" onSubmit={localStorage.getItem('namaHewan1') === null ? handleSubmit1 : localStorage.getItem('namaHewan2') === null ? handleSubmit2 : localStorage.getItem('namaHewan3') === null ? handleSubmit3 : handleSubmit4}>
                            <label htmlFor="namaHewan">Nama Hewan Peliharaan Kamu</label><br />
                            <input type="text" placeholder='Nama Hewan Peliharaan' onChange={e => handlerName(e, 'value')} /><br />
                            <label htmlFor="PilihHewan">Pilih Hewan Peliharaan</label><br />

                            <select className="pets-dropdown" name="pets" onChange={e => handlerSpesies(e, 'value')}>
                                <option value="invalid">Pilih Hewan Peliharaan</option>
                                <option value="Anjing">Anjing</option>
                                <option value="Kucing">Kucing</option>
                                <option value="Burung">Burung</option>
                                <option value="Kelinci">Kelinci</option>
                                <option value="Reptil">Reptil</option>
                                <option value="Lainnya">Lainnya</option>
                            </select> <br />
                            <label htmlFor="gender">Pilih Jenis Kelamin Peliharaan Kamu</label><br />
                            <select className="pets-dropdown" id="pets" name="pets" onChange={e => handlerGender(e, 'value')}>
                                <option value="Choose">Pilih Jenis Kelamin</option>
                                <option value="Jantan">Jantan</option>
                                <option value="Betina">Betina</option>
                            </select> <br />



                        </form>
                    </ModalBody>
                    <ModalFooter className='modal-footer-pet'>
                        <button onClick={localStorage.getItem('namaHewan1') === null ? handleSubmit1 : localStorage.getItem('namaHewan2') === null ? handleSubmit2 : localStorage.getItem('namaHewan3') === null ? handleSubmit3 : handleSubmit4} className='action-pet btn-block'>Tambahkan</button>
                    </ModalFooter>
                </Modal>
            </div>
        </div>
    );
}

export default ModalExample;