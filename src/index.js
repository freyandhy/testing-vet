import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import App from "./App";
import "bootstrap/dist/css/bootstrap.css";
import "jquery/dist/jquery";
import "popper.js";
import "bootstrap/dist/js/bootstrap";
import { createStore } from "redux";
import { Provider } from "react-redux";


const globalState = {
  doctor: 0,
};

// Reducer
const rootReducer = (state = globalState, action) => {
  if (action.type === "ADD_DOCTOR") {
    return {
      ...state,
      doctor: state.doctor + 1,
    };
  }

  if (action.type === "DELETE_DOCTOR") {
    return {
      ...state,
      doctor: state.doctor + 1,
    };
  }
  return state;
};

// Store
const globalStore = createStore(rootReducer);

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={globalStore}>
        <App />
      </Provider>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);
