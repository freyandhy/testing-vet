import React, { useState } from "react";
import Header from "../layout/Header";
import { Switch, Route, NavLink, useRouteMatch } from "react-router-dom";
import "./UserHistory.css";
import { Container, Row, Col } from "reactstrap";
import UserApp from "../components/UserApp";
import UserHis from "../components/UserHis";
import EditUserRight from "../components/EditUserRight";

const UserHistory = () => {
  let { path, url } = useRouteMatch();
  const [editProfile, setEditProfile] = useState(false);

  const handleEditProfile = () => {
    setEditProfile(!editProfile);
  };

  //ada komponen book from patient default
  //ada komponen approval clicked

  return (
    <>
      <Header />
      <div className="dokter-background">
        <br />
        <br />
        <br />
        <br />
        <br />
        <Container fluid={true}>
          <Row>
            <Col sm="4" className="dokter-left-box ">
              <div className="leftSide text-center mx-5">
                <div>
                  <img
                    src="https://ik.imagekit.io/zvqf9loqe2/VET/Ellipse_10_tm2bFcdD2lB.svg"
                    width="30%"
                    className="mt-4"
                  />
                  <div className="profile-name">
                    <h3>Alexandria Raihan</h3>
                    <div className="container">
                      <div className="statusDoc btn doctor-btn align-items-center">{`USER`}</div>
                    </div>
                  </div>
                </div>

                <Row className="profile1-comp mt-4">
                  <Col>
                    <img
                      src="https://ik.imagekit.io/zvqf9loqe2/VET/ic_round-pets_wx0wyUcOP-G.svg"
                      width="18%"
                    ></img>
                    <h5>4 Pets</h5>
                  </Col>
                  <Col>
                    <img
                      src="https://ik.imagekit.io/zvqf9loqe2/VET/Vector_gObBYmwCQpty.svg"
                      width="18%"
                    ></img>
                    <h5>3 Times</h5>
                  </Col>
                </Row>

                <hr style={{ marginRight: "50px", marginLeft: "50px" }} />
                <NavLink
                  to={`${url}/edit-profile`}
                  exact={true}
                  className="nav-link inactive-nav-edit-profile"
                  activeClassName="active-nav-edit-profile"
                  onClick={handleEditProfile}
                >
                  Edit Profile
                </NavLink>
                <br />
              </div>

              <div className="logout mx-5 mb-5">
                <Row className="logout-comp">
                  <img
                    src="https://ik.imagekit.io/zvqf9loqe2/VET/clarity_logout-line_6JOgslcDRtq.svg"
                    className="ml-5"
                  />
                  <div className="text">
                    <h5>{`Logout`}</h5>
                  </div>
                </Row>
              </div>
            </Col>
            <Col sm="8">
              <div className="dokter-right-box">
                {editProfile && (
                  <Switch>
                    <Route path={`${url}/edit-profile`}>
                      <EditUserRight />
                    </Route>
                  </Switch>
                )}
                {!editProfile && (
                  <>
                    <Row>
                      <Row className="mt-4 ml-4">
                        <NavLink
                          to={url}
                          exact={true}
                          className="nav-link inactive-nav mr-4"
                          activeClassName="active-nav"
                        >
                          Appointment
                        </NavLink>
                        <NavLink
                          to={`${url}/user-history`}
                          exact={true}
                          className="nav-link inactive-nav ml-4"
                          activeClassName="active-nav"
                        >
                          History
                        </NavLink>
                      </Row>
                    </Row>
                    <hr />
                  </>
                )}

                <Row className="mb-4">
                  <Switch>
                    <Route exact path={`${path}`}>
                      <UserApp />
                    </Route>
                  </Switch>
                  <Switch>
                    <Route exact path={`${path}/user-history`}>
                      <UserHis />
                    </Route>
                  </Switch>
                </Row>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default UserHistory;
