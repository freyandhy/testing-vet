import { SettingsSystemDaydreamTwoTone } from '@material-ui/icons';
import React, { useEffect, useState } from 'react';
import Axios from 'axios'
import {
  Row,
  Label,
  Form,
  FormGroup,
  Input,
  Button,
  Modal,
  ModalBody,
} from 'reactstrap';
import {connect} from 'react-redux'

const EditUserRight = (props) => {
  const checklist =
    'https://ik.imagekit.io/zvqf9loqe2/VET/Group_DcxThd9dOuTC.svg';
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  const [data, setData] = useState([])

  useEffect(() => {
    Axios.get('https://5fc6be8cf3c77600165d7887.mockapi.io/user')
      .then(res => setData(res.data[0]))
  }, [props.user])

  const [username, setUsername] = useState('')
  const [gender, setGender] = useState('')
  const [phone, setPhone] = useState('')
  const [email, setEmail] = useState('')
  const [pets, setPets] = useState('')
  const [frequency, setFrequency] = useState('')
  const handleChangeProfile = () => {

  const bodyData = {
    username: username,
    gender: gender,
    phone: phone,
    email: email,
    pets: pets,
    frequency: frequency
  }

    axios(`https://5fc6be8cf3c77600165d7887.mockapi.io/user/1`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      }, data: JSON.stringify(bodyData)
    })
      .then(() => {
        axios.get('https://5fc6be8cf3c77600165d7887.mockapi.io/user')
        .then(res=>setData(res.data[0]))
      })
      
  }



  return (
    <div className="rightSide">
      <Form>
        <div className="rightSide">
          <Form>
            <div>
              <h3 className="form">Upload Photo</h3>
              <hr />
              <div className="rcorners">
                <div className="upload">
                  <img src="https://ik.imagekit.io/zvqf9loqe2/VET/Vector__1__wVzNtBzycat.svg" />
                </div>
                <div className="upload-text">
                  <h5 style={{ color: '#818181' }}>Upload Image</h5>
                </div>
              </div>

              <h3 style={{ marginTop: '40px' }} className="form">
                Basic Information
              </h3>
              <hr />
              <Label className="form">Username</Label>
              <br />
              <div className="form1">
                <Input
                  className="input"
                  type="text"
                  placeholder="Alexandria"
                  defaultValue={data.username===null ? data.username : ''}
                />
              </div>

              <br />
              <Label className="form">Gender</Label>
              <br />
              <Row className="radioButton">
                <FormGroup check>
                  {data.gender === 'male' ?
                    <div>
                      <Label
                        style={{ marginLeft: '-34px', marginRight: '80px' }}
                        check>
                        <Input type="radio" name="radio1" checked />Male</Label>

                      <Label check>
                        <Input type="radio" name="radio1" />Female</Label>
                    </div> :
                    <div>
                    <Label
                      style={{ marginLeft: '-34px', marginRight: '80px' }}
                      check>
                      <Input type="radio" name="radio1" />Male</Label>

                    <Label check>
                      <Input type="radio" name="radio1" checked />Female</Label>
                  </div> 
                }
                </FormGroup>
              </Row>

              <br />
              <br />
              <h3 className="form">Contact Details</h3>
              <hr />
              <Label className="form">Nomor Telephone</Label>
              <br />
              <div className="form1">
                <Input
                  className="input"
                  type="telephone"
                  placeholder="+712371723 18231823"
                  defaultValue={data.telepon}
                />
              </div>

              <br />
              <Label className="form">Email</Label>
              <br />
              <div className="form1">
                <Input
                  className="input"
                  type="email"
                  placeholder="alexanda@gmail.com"
                  defaultValue={data.email}
                />
              </div>

              <br />
              <br />

              <h4 className="form">Informasi tentang Hewan Peliharaan</h4>
              <br />
              <Label className="form">Jumlah Hewan Peliharaan</Label>
              <div className="form1">
                <Input
                  className="input"
                  type="jumlahhewan"
                  placeholder={data.pets}
                />
              </div>

              <br />
              <Label className="form">Waktu Berkunjung Perawatan</Label>
              <div className="form1">
                <Input
                  className="input"
                  type="waktuberkunjung"
                  placeholder={data.frekuensi}
                />
              </div>

              <br />
              <br />
              <br />
            </div>
          </Form>
        </div>
      </Form>

      <div>
        <Button
          className="tombol-simpan"
          type="simpan"
          color="warning"
          onClick={handleChangeProfile}>
          Simpan
        </Button>
        <Modal
          isOpen={modal}
          toggle={toggle}
          style={{ marginTop: '13rem', borderRadius: '25px' }}>
          <ModalBody
            style={{
              backgroundColor: '#1A3150',
              paddingTop: '3rem',
              paddingBottom: '3rem',
              borderRadius: '2px',
            }}>
            <div className="col ">
              <div className="row d-flex justify-content-center">
                <img src={checklist} />
              </div>

              <br />
              <div className="row d-flex justify-content-center">
                <h1 style={{ color: 'white' }}>Update Sukses</h1>
              </div>
            </div>
          </ModalBody>
        </Modal>
      </div>

      <br />
      <br />
    </div>
  );
};

const stateToProps = (globalState) => {
  return {
    user: globalState.doctor
  };
};

export default connect (stateToProps)(EditUserRight);
