import Axios from 'axios';
import React, { useState, useEffect } from 'react';
import {
  Row,
  Label,
  Form,
  FormGroup,
  Input,
  Button,
  Modal,
  ModalBody,
} from 'reactstrap';
import UploadFoto from './UploadFoto'

import axios from 'axios';

const EditClinicRight = () => {
  const checklist =
    'https://ik.imagekit.io/zvqf9loqe2/VET/Group_DcxThd9dOuTC.svg';
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);

  // const [isLoading, setIsLoading] = useState(false);

  // const [clinics, setClinics] = useState('');

  // useEffect(() => {
  //   setIsLoading(true);

  //   Axios(`http://13.236.9.153/admin/clinic`, {
  //     method: 'GET',
  //     headers: {
  //       'Content-Type': 'application/json',
  //       Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjFjODk5NmU2LTFlZjctNDkxMi1iOGUyLWYwOTllYjI0ZTM3ZiIsInVzZXJuYW1lIjoicnNodG1paSIsImZ1bGxuYW1lIjoiUlNIIFRNSUkiLCJlbWFpbCI6InJzaHRtaWlAZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmIkMTAkbThOVjhVdkJhbEtmSjdaVEtMekNLZVRZNVk1WURNby5YeG85Q09TMjBmSEF2SmI3OUxaMWEiLCJwaG9uZU51bWJlciI6bnVsbCwiZ2VuZGVyIjpudWxsLCJwaWN0dXJlVXJsIjpudWxsLCJyb2xlIjoiYWRtaW4iLCJjcmVhdGVkQXQiOiIyMDIwLTEyLTA1VDA2OjIyOjA0LjAwMFoiLCJ1cGRhdGVkQXQiOiIyMDIwLTEyLTA1VDA2OjIyOjA0LjAwMFoiLCJjbGluaWNJZCI6bnVsbCwiaWF0IjoxNjA3MTQ5MzQxLCJleHAiOjE2MDcxNzA5NDF9.XfQbuVXxGDfs11nQK5Hvf5FUL_DKtR9I8Npc2vKEOGk`,
  //     },
  //   }).then((res) => {
  //     setClinics(res.data);
  //     setIsLoading(false);
  //   });
  // }, []);

  // const data = {
  //   username: username,
  //   password: password,
  // };

  // axios
  //   .patch('http://13.236.9.153/admin/clinic?id=abc', data, {
  //     'Content-Type': 'application/json',
  //     Authorization: `Bearer ${localStorage.getItem('token')}`,
  //   })
  //   .then((ress) => {
  //     console.log(ress.data);
  //     history.push('/');
  //     localStorage.setItem('id', ress.data.id);
  //     localStorage.setItem('username', ress.data.username);
  //     localStorage.setItem('fullname', ress.data.fullname);
  //     localStorage.setItem('email', ress.data.email);
  //     localStorage.setItem('password', ress.data.password);
  //     localStorage.setItem('phoneNumber', ress.data.phoneNumber);
  //     localStorage.setItem('gender', ress.data.gender);
  //     localStorage.setItem('pictureurl', ress.data.pictureUrl);
  //     localStorage.setItem('role', ress.data.role);
  //     localStorage.setItem('clinicid', ress.data.clinicId);
  //   });

  return (
    <div>
      {/* {isLoading === true ? (
        <h1>Loading...</h1>
      ) : ( */}
      <div className="rightSide">
        <Form>
          <div>
            <h3 className="form">Upload Photo</h3>

            <hr />
            <br />
            <div className="row d-flex p-4" style={{ paddingRight: '100px' }}>
              <div className="col">
                <div className="rcorners">
                  <div className="upload">
                    <UploadFoto />
                  </div>
                  <div className="upload-text">
                    <h5 style={{ color: '#818181' }}>Upload Image</h5>
                  </div>
                </div>
              </div>
              <div className="col" style={{ paddingRight: '120px' }}>
                <h5>
                  <b>About</b>
                </h5>
                <p style={{ textAlign: 'justify' }}>Keterangan Klinik</p>
              </div>
            </div>

            <h3 className="form">Clinic Information</h3>
            <hr />

            <Label className="form">Status</Label>
            <br />
            {/* <Row className="radioButton">
              {clinics.isOpen === true ? (
                <FormGroup check>
                  <Label
                    style={{ marginLeft: '-34px', marginRight: '80px' }}
                    check>
                    <Input type="radio" name="radio1" checked />
                    Online
                  </Label>

                  <Label check>
                    <Input type="radio" name="radio1" />
                    Offline
                  </Label>
                </FormGroup>
              ) : (
                <FormGroup check>
                  <Label
                    style={{ marginLeft: '-34px', marginRight: '80px' }}
                    check>
                    <Input type="radio" name="radio1" />
                    Online
                  </Label>

                  <Label check>
                    <Input type="radio" name="radio1" checked />
                    Offline
                  </Label>
                </FormGroup>
              )}
            </Row>

            <br /> */}
            <br />

            <Row>
              <div className="d-flex">
                <FormGroup className="form" style={{ marginLeft: '-10px' }}>
                  <Label for="exampleSelect">Waktu Buka</Label>
                  <Input
                    style={{ width: '200px' }}
                    type="select"
                    name="select"
                    id="exampleSelect">
                    <option>Pilih jam</option>
                    <option>08.00</option>
                    <option>09.00</option>
                    <option>10.00</option>
                    <option>11.00</option>
                    <option>12.00</option>
                    <option>13.00</option>
                    <option>14.00</option>
                    <option>15.00</option>
                  </Input>
                </FormGroup>

                <FormGroup className="form">
                  <Label for="exampleSelect">Waktu Tutup</Label>
                  <Input
                    style={{ width: '200px' }}
                    type="select"
                    name="select"
                    id="exampleSelect">
                    {/* <option>{clinics.jamTutup}</option> */}
                    <option>11.00</option>
                    <option>12.00</option>
                    <option>13.00</option>
                    <option>14.00</option>
                    <option>15.00</option>
                    <option>16.00</option>
                    <option>17.00</option>
                    <option>18.00</option>
                    <option>19.00</option>
                    <option>20.00</option>
                    <option>21.00</option>
                  </Input>
                </FormGroup>
              </div>
            </Row>

            <br />
            <br />
            <h3 className="form">Basic Information</h3>
            <hr />

            <Label className="form">Clinic Name</Label>
            <br />
            <div className="form1">
              <Input
                className="input"
                type="text"
                placeholder="RS Dihajar"
                defaultValue={localStorage.getItem('fullname')}></Input>
            </div>

            <br />
            <Label className="form">Facilities</Label>

            <div className="form">
              <FormGroup check>
                <Label check>
                  <Input
                    type="checkbox"
                    // checked={
                    //   clinics &&
                    //   clinics.facilities.includes('Health Consultation')
                    // }
                  />{' '}
                  Health Consultation
                </Label>
              </FormGroup>

              <FormGroup check>
                <Label check>
                  <Input
                    type="checkbox"
                    // checked={
                    //   clinics && clinics.facilities.includes('Hospitalization')
                    // }
                  />{' '}
                  Hospitalization
                </Label>
              </FormGroup>

              <FormGroup check>
                <Label check>
                  <Input
                    type="checkbox"
                    // checked={
                    //   clinics && clinics.facilities.includes('Physiotheraphy')
                    // }
                  />{' '}
                  Physiotheraphy
                </Label>
              </FormGroup>

              <FormGroup check>
                <Label check>
                  <Input
                    type="checkbox"
                    // checked={
                    //   clinics && clinics.facilities.includes('Dental Care')
                    // }
                  />{' '}
                  Dental Care
                </Label>
              </FormGroup>

              <FormGroup check>
                <Label check>
                  <Input
                    type="checkbox"
                    // checked={
                    //   clinics &&
                    //   clinics.facilities.includes('Laboratory Service')
                    // }
                  />{' '}
                  Laboratory Service
                </Label>
              </FormGroup>
            </div>

            <br />
            <Label className="form">Hewan Peliharaan</Label>

            <div className="form">
              <FormGroup check>
                <Label check>
                  <Input
                    type="checkbox"
                    // checked={clinics && clinics.pasien.includes('anjing')}
                  />{' '}
                  Anjing
                </Label>
              </FormGroup>

              <FormGroup check>
                <Label check>
                  <Input
                    type="checkbox"
                    // checked={clinics && clinics.pasien.includes('kucing')}
                  />{' '}
                  Kucing
                </Label>
              </FormGroup>

              <FormGroup check>
                <Label check>
                  <Input
                    type="checkbox"
                    // checked={clinics && clinics.pasien.includes('kelinci')}
                  />{' '}
                  Kelinci
                </Label>
              </FormGroup>

              <FormGroup check>
                <Label check>
                  <Input
                    type="checkbox"
                    // checked={clinics && clinics.pasien.includes('Reptil')}
                  />{' '}
                  Reptil
                </Label>
              </FormGroup>

              {/* <FormGroup check>
                  <Label check>
                    <div className="row">
                      <div className="col" style={{ marginLeft: '-16px' }}>
                        <Input type="checkbox" /> Lainnya:
                      </div>

                      <div
                        className="form1 col"
                        style={{ marginLeft: '-106px' }}>
                        <Input
                          className="input"
                          type="text"
                          placeholder="jenis hewan"
                        />
                      </div>
                    </div>
                  </Label>
                </FormGroup> */}
            </div>

            <br />
            <br />
            <h3 className="form">Contact Details</h3>
            <hr />

            <Label className="form">Telephone</Label>
            <br />
            <div className="form1">
              <Input
                className="input"
                type="telephone"
                placeholder="+712371723 18231823"
                defaultValue={localStorage.getItem('phoneNumber')}
              />
            </div>

            <br />
            <Label className="form">Email</Label>
            <br />
            <div className="form1">
              <Input
                className="input"
                type="email"
                placeholder="alexanda@gmail.com"
                defaultValue={localStorage.getItem('email')}
              />
            </div>

            <br />

            <Label className="form">Address</Label>
            <br />
            <div className="row" style={{ paddingLeft: '30px' }}>
              <div className="form1 col">
                <Input
                  style={{ width: '400px' }}
                  className="input"
                  type="text"
                  placeholder="Jalan Paguyuban"
                  // defaultValue={clinics.address}
                />
              </div>
              <div className="form1 col">
                <Input
                  style={{ width: '200px' }}
                  className="input"
                  type="text"
                  placeholder="Jakarta"
                  // defaultValue={clinics.city}
                />
              </div>
            </div>

            <br />
            <br />
          </div>
        </Form>
      </div>

      <div>
        <Button
          className="tombol-simpan"
          type="simpan"
          color="warning"
          onClick={toggle}>
          Simpan
        </Button>
        <Modal isOpen={modal} toggle={toggle} style={{ marginTop: '20rem' }}>
          <ModalBody
            style={{
              backgroundColor: '#1A3150',
              paddingTop: '4rem',
            }}>
            <div className="col ">
              <div className="row d-flex justify-content-center">
                <img src={checklist} />
              </div>

              <br />
              <div className="row d-flex justify-content-center">
                <h1 style={{ color: 'white' }}>Update Sukses</h1>
              </div>
            </div>
          </ModalBody>
        </Modal>
      </div>
      <br />
      <br />
    </div>
  );
};

export default EditClinicRight;
