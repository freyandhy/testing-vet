import React, { useState, useEffect } from "react";
import { Container, Row, Col, CardBody, CardText } from "reactstrap";
import "./UserHis.css";
import axios from "axios";
import { useHistory } from "react-router-dom";

const UserHis = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [userHistories, setUserHistories] = useState([]);
  const body = {};

  useEffect(() => {
    const url = "https://5fc6be8cf3c77600165d7887.mockapi.io/history";
    axios.get(url).then((res) => {
      setUserHistories(res.data);
    });
  }, []);

  console.log(userHistories);

  const handleBookAgain = (e) => {
    setIsLoading(true);
    e.preventDefault();
    axios.post("https://5fbcb4963f8f90001638c271.mockapi.io/bookings", body);
    setIsLoading(false);
  };

  return userHistories.map((userHistory) => {
    return (
      <Container key={userHistory.id}>
        <div tabIndex="1" className="card w-100 mb-2">
          <CardBody>
            <Row className="text-center">
              <Col>
                <div className="d-flex align-items-center">
                  <img src="https://ik.imagekit.io/zvqf9loqe2/VET/image_4__1__bFMSOw1cFmCM.svg" />
                </div>
              </Col>

              <Col className="d-flex align-items-center justify-content-center">
                <div className="clinic-name">Klinik Kehewanan</div>
              </Col>
              <Col className="d-flex align-items-center justify-content-center">
                <div className="clinic-name">{userHistory.tgl}</div>
              </Col>
              <Col className="d-flex align-items-center justify-content-center">
                <div className="history-time">Finished</div>
              </Col>
              <Col sm="4">
                <span
                  className="mr-1 btn btn-warning"
                  onClick={handleBookAgain}
                >
                  Book Again
                </span>
              </Col>
            </Row>
          </CardBody>
        </div>
      </Container>
    );
  });
};

export default UserHis;
