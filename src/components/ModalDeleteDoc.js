import React from 'react'
import { useState } from 'react'
import { useHistory } from 'react-router-dom';
import { Button, Modal, ModalBody, ModalFooter } from "reactstrap";
import axios from 'axios'
import './ModalDeleteDoc.css'
import {connect} from 'react-redux'


const ModalDeleteDoc = (props) => {
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);
    const history = useHistory()
  

    const deleteDoctor = () => {
        console.log(props.id)
        axios(`https://5fc6be8cf3c77600165d7887.mockapi.io/managedoc/${props.id}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
        })
            .then(() => {
                history.push('/managedoctor/manage-doctor')
                props.deleteVet()
                toggle()
            })
    }

    return (
        <div>
            <img className='deleteDoc' src="https://ik.imagekit.io/zvqf9loqe2/VET/Vector__5__hJPCC-fLDIp.svg" style={{ paddingTop: '1rem', paddingLeft: '8rem' }} onClick={toggle} />
            <Modal isOpen={modal} toggle={toggle} className="doctor-modal-delete">
                <ModalBody className="text-center">{`Are you sure to delete ${props.username} from your clinic?`}</ModalBody>
                <ModalFooter>
                    <Button className='btn-delete-doc' onClick={deleteDoctor}>Delete</Button>
                    <Button className='btn-cancel-delete-doc' onClick={toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

const stateToProps = (globalState) => {
    return {
      doctor: globalState.doctor
    }
  }

const dispatcher = (dispatch) => {
    return {
        deleteVet: () => dispatch({ type: "ADD_DOCTOR" })
    } 
}

export default connect(stateToProps, dispatcher) (ModalDeleteDoc);