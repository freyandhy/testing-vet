import Axios from "axios";
import { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import Navbar from "../layout/Header";
import gambar from "../img/image 4 (1).png";
import "./bookingDetails.css";
import profilDoc from "../img/Rectangle 27.png";
import cek from "../img/check-mark.svg";
import add from "../img/Vector (1).png";
import ModalPetInfo from "./ModalPetInfo";
import anjing from "../img/emojione_dog-face.png";
import close from "../img/Vector (2).png";
import Footer from "../layout/Footer";

const BookingDetails = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [clinics, setClinics] = useState([]);
  const [doctors, setDoctors] = useState([]);
  const [pet, setPet] = useState("");

  useEffect(() => {
    setIsLoading(true);
    Axios.get("https://5fbcb4963f8f90001638c271.mockapi.io/search").then(
      (res) => {
        setClinics(res.data);
      }
    );
    Axios.get("https://5fbcb4963f8f90001638c271.mockapi.io/dokter").then(
      (res) => {
        setDoctors(res.data);
        setIsLoading(false);
      }
    );
  }, []);

  const params = useParams();
  //   console.log(params);

  let namaHari = [
    "Minggu",
    "Senin",
    "Selasa",
    "Rabu",
    "Kamis",
    "Jumat",
    "Sabtu",
    "Minggu",
    "Senin",
    "Selasa",
    "Rabu",
    "Kamis",
    "Jumat",
    "Sabtu",
    "Minggu",
  ];
  let namaBulan = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "Mei",
    "Jun",
    "Jul",
    "Agu",
    "Sep",
    "Okt",
    "Nov",
    "Des",
  ];

  let tglbase1 = new Date();
  tglbase1.setDate(tglbase1.getDate() + 1);
  let tgl1 = tglbase1.getDate();
  let namaHari1 = namaHari[tglbase1.getDay()];
  let namaBulan1 = namaBulan[tglbase1.getMonth()];

  let tglbase2 = new Date();
  tglbase2.setDate(tglbase2.getDate() + 2);
  let tgl2 = tglbase2.getDate();
  let namaHari2 = namaHari[tglbase2.getDay()];
  let namaBulan2 = namaBulan[tglbase2.getMonth()];

  let tglbase3 = new Date();
  tglbase3.setDate(tglbase3.getDate() + 3);
  let tgl3 = tglbase3.getDate();
  let namaHari3 = namaHari[tglbase3.getDay()];
  let namaBulan3 = namaBulan[tglbase3.getMonth()];

  let tglbase4 = new Date();
  tglbase4.setDate(tglbase4.getDate() + 4);
  let tgl4 = tglbase4.getDate();
  let namaHari4 = namaHari[tglbase4.getDay()];
  let namaBulan4 = namaBulan[tglbase4.getMonth()];

  let tglbase5 = new Date();
  tglbase5.setDate(tglbase5.getDate() + 5);
  let tgl5 = tglbase5.getDate();
  let namaHari5 = namaHari[tglbase5.getDay()];
  let namaBulan5 = namaBulan[tglbase5.getMonth()];

  let tglbase6 = new Date();
  tglbase6.setDate(tglbase6.getDate() + 6);
  let tgl6 = tglbase6.getDate();
  let namaHari6 = namaHari[tglbase6.getDay()];
  let namaBulan6 = namaBulan[tglbase6.getMonth()];

  let tglbase7 = new Date();
  tglbase7.setDate(tglbase7.getDate() + 7);
  let tgl7 = tglbase7.getDate();
  let namaHari7 = namaHari[tglbase7.getDay()];
  let namaBulan7 = namaBulan[tglbase7.getMonth()];

  const [jamMulaiDok, setJamMulaiDok] = useState(100);
  const [jamPulangDok, setJamPulangDok] = useState("");
  const [waktuKunjung, setWaktuKunjung] = useState(0);
  const handlerJam = (e) => {
    setWaktuKunjung(e.target.value);
    setJamMulaiDok(e.target.value.slice(0, 5));
    setJamPulangDok(e.target.value.slice(6, 11));
    localStorage.setItem("waktuKunjungan", e.target.value);
  };

  const [hari, setHari] = useState("");
  const handlerHari = (e) => {
    localStorage.setItem("hariKunjungan", e.target.id);
    setHari(e.target.value);
  };

  const history = useHistory();
  const handlerDelete = (e) => {
    localStorage.removeItem(`namaHewan${e.target.id}`);
    history.push(`/book-clinic/${params.id}`);
  };

  const [dokter, setDokter] = useState("");
  const handlerDokter = (e) => {
    setDokter(e.target.id);
    localStorage.setItem("namaDokter", e.target.id);
  };

  return (
    <div>
      <Navbar /> <br />
      <br />
      <br />
      <br />
      {isLoading === true ? (
        <div class="spinner">
          <div class="bounce1"></div>
          <div class="bounce2"></div>
          <div class="bounce3"></div>
        </div>
      ) : (
        clinics
          .filter((clinic) => clinic.id === params.id)
          .map((clinic) => (
            <div>
              <div className="container">
                <div>
                  {localStorage.setItem("namaRS", clinic.namaKlinik)}
                  <div className="yangdiAtas">
                    <h1 className="namaRS">{clinic.namaKlinik}</h1>
                  </div>
                  <h3 className="informasi-umum">Informasi Umum</h3>
                  <div className="row">
                    <div className="col-7 aboutRS">
                      <img src={gambar} alt="" />
                      <h3>Tentang</h3>
                      <p>{clinic.tentang}</p>
                    </div>
                    <div className="col-5 info-kunjungan">
                      <h3>Informasi Kunjungan</h3>
                      <p>Hari Kunjungan</p>
                      <div className="hari-kunjungan">
                        <div className="d-flex">
                          <button
                            id={`${namaHari1} ${tgl1} ${namaBulan1}`}
                            value={namaHari1}
                            onClick={(e) => {
                              handlerHari(e, "value");
                            }}
                            className={`harkun ${
                              hari === namaHari1 ? "harkun-active" : ""
                            }`}
                          >
                            {namaHari1} <br /> {tgl1} {namaBulan1}
                          </button>
                          <button
                            value={namaHari2}
                            id={`${namaHari2} ${tgl2} ${namaBulan2}`}
                            onClick={(e) => {
                              handlerHari(e, "value");
                            }}
                            className={`harkun ${
                              hari === namaHari2 ? "harkun-active" : ""
                            }`}
                          >
                            {namaHari2} <br /> {tgl2} {namaBulan2}
                          </button>
                          <button
                            value={namaHari3}
                            onClick={(e) => {
                              handlerHari(e, "value");
                            }}
                            className={`harkun ${
                              hari === namaHari3 ? "harkun-active" : ""
                            }`}
                            id={`${namaHari3} ${tgl3} ${namaBulan3}`}
                          >
                            {namaHari3} <br /> {tgl3} {namaBulan3}
                          </button>
                          <button
                            value={namaHari4}
                            onClick={(e) => {
                              handlerHari(e, "value");
                            }}
                            className={`harkun ${
                              hari === namaHari4 ? "harkun-active" : ""
                            }`}
                            id={`${namaHari4} ${tgl4} ${namaBulan4}`}
                          >
                            {namaHari4} <br /> {tgl4} {namaBulan4}
                          </button>
                        </div>
                        <div className="d-flex">
                          <button
                            value={namaHari5}
                            onClick={(e) => {
                              handlerHari(e, "value");
                            }}
                            className={`harkun ${
                              hari === namaHari5 ? "harkun-active" : ""
                            }`}
                            id={`${namaHari5} ${tgl5} ${namaBulan5}`}
                          >
                            {namaHari5} <br /> {tgl5} {namaBulan5}
                          </button>
                          <button
                            value={namaHari6}
                            onClick={(e) => {
                              handlerHari(e, "value");
                            }}
                            className={`harkun ${
                              hari === namaHari6 ? "harkun-active" : ""
                            }`}
                            id={`${namaHari6} ${tgl6} ${namaBulan6}`}
                          >
                            {namaHari6} <br /> {tgl6} {namaBulan6}
                          </button>
                          <button
                            value={namaHari7}
                            onClick={(e) => {
                              handlerHari(e, "value");
                            }}
                            className={`harkun ${
                              hari === namaHari7 ? "harkun-active" : ""
                            }`}
                            id={`${namaHari7} ${tgl7} ${namaBulan7}`}
                          >
                            {namaHari7} <br /> {tgl7} {namaBulan7}
                          </button>
                        </div>
                      </div>
                      <div className="waktu-kunjung">
                        <p>Waktu Kunjungan</p>
                        {clinic.jamBuka.slice(0, 2) < 12 &&
                        clinic.jamTutup.slice(0, 2) < 16 ? (
                          <div className="d-flex">
                            <button
                              value={`${clinic.jamBuka}-12.00`}
                              onClick={(e) => {
                                handlerJam(e, "value");
                              }}
                              className={`wakun-btn ${
                                waktuKunjung === `${clinic.jamBuka}-12.00`
                                  ? "wakun-btn-active"
                                  : ""
                              }`}
                            >
                              {clinic.jamBuka}-12.00 Pagi
                            </button>
                            <button
                              value={`12.00-${clinic.jamTutup}`}
                              onClick={(e) => {
                                handlerJam(e, "value");
                              }}
                              className={`wakun-btn ${
                                waktuKunjung === `12.00-${clinic.jamTutup}`
                                  ? "wakun-btn-active"
                                  : ""
                              }`}
                            >
                              12.00-{clinic.jamTutup} Siang
                            </button>
                            <br />
                          </div>
                        ) : clinic.jamBuka.slice(0, 2) < 12 &&
                          clinic.jamTutup.slice(0, 2) > 18 ? (
                          <div>
                            <div className="d-flex">
                              <button
                                value={`${clinic.jamBuka}-12.00`}
                                onClick={(e) => {
                                  handlerJam(e, "value");
                                }}
                                className={`wakun-btn ${
                                  waktuKunjung === `${clinic.jamBuka}-12.00`
                                    ? "wakun-btn-active"
                                    : ""
                                }`}
                              >
                                {clinic.jamBuka}-12.00 Pagi
                              </button>
                              <button
                                value={`12.00-15.00`}
                                onClick={(e) => {
                                  handlerJam(e, "value");
                                }}
                                className={`wakun-btn ${
                                  waktuKunjung === "12.00-15.00"
                                    ? "wakun-btn-active"
                                    : ""
                                }`}
                              >
                                12.00-15.00 Siang
                              </button>
                              <br />
                            </div>
                            <div className="d-flex">
                              <button
                                value={`15.00-18.00`}
                                onClick={(e) => {
                                  handlerJam(e, "value");
                                }}
                                className={`wakun-btn ${
                                  waktuKunjung === "15.00-18.00"
                                    ? "wakun-btn-active"
                                    : ""
                                }`}
                              >
                                15.00-18.00 Sore{" "}
                              </button>
                              <button
                                value={`18.00-${clinic.jamTutup}`}
                                onClick={(e) => {
                                  handlerJam(e, "value");
                                }}
                                className={`wakun-btn ${
                                  waktuKunjung === `18.00-${clinic.jamTutup}`
                                    ? "wakun-btn-active"
                                    : ""
                                }`}
                              >
                                18.00-{clinic.jamTutup} Malam{" "}
                              </button>
                            </div>
                          </div>
                        ) : clinic.jamBuka.slice(0, 2) < 12 &&
                          clinic.jamTutup.slice(0, 2) < 19 ? (
                          <div>
                            <div className="d-flex">
                              <button
                                value={`${clinic.jamBuka}-12.00`}
                                onClick={(e) => {
                                  handlerJam(e, "value");
                                }}
                                className={`wakun-btn ${
                                  waktuKunjung === `${clinic.jamBuka}-12.00`
                                    ? "wakun-btn-active"
                                    : ""
                                }`}
                              >
                                {clinic.jamBuka}-12.00 Pagi
                              </button>
                              <button
                                value={`12.00-15.00`}
                                onClick={(e) => {
                                  handlerJam(e, "value");
                                }}
                                className={`wakun-btn ${
                                  waktuKunjung === `12.00-15.00`
                                    ? "wakun-btn-active"
                                    : ""
                                }`}
                              >
                                12.00-15.00 Siang
                              </button>
                              <br />
                            </div>
                            <div className="d-flex">
                              <button
                                value={`15.00-${clinic.jamTutup}`}
                                onClick={(e) => {
                                  handlerJam(e, "value");
                                }}
                                className={`wakun-btn ${
                                  waktuKunjung === `15.00-${clinic.jamTutup}`
                                    ? "wakun-btn-active"
                                    : ""
                                }`}
                              >
                                15.00-{clinic.jamTutup} Sore{" "}
                              </button>
                            </div>
                          </div>
                        ) : clinic.jamBuka.slice(0, 2) > 12 &&
                          clinic.jamTutup.slice(0, 2) < 19 ? (
                          <div className="d-flex">
                            <button
                              value={`${clinic.jamBuka}-15.00`}
                              onClick={(e) => {
                                handlerJam(e, "value");
                              }}
                              className={`wakun-btn ${
                                waktuKunjung === `${clinic.jamBuka}-15.00`
                                  ? "wakun-btn-active"
                                  : ""
                              }`}
                            >
                              {clinic.jamBuka}-15.00 Siang
                            </button>
                            <br />
                            <button
                              value={`15.00-${clinic.jamTutup}`}
                              onClick={(e) => {
                                handlerJam(e, "value");
                              }}
                              className={`wakun-btn ${
                                waktuKunjung === `15.00-${clinic.jamTutup}`
                                  ? "wakun-btn-active"
                                  : ""
                              }`}
                            >
                              15.00-{clinic.jamTutup} Sore{" "}
                            </button>
                          </div>
                        ) : (
                          <div>
                            <div className="d-flex">
                              <button
                                value={`${clinic.jamBuka}-15.00`}
                                onClick={(e) => {
                                  handlerJam(e, "value");
                                }}
                                className={`wakun-btn ${
                                  waktuKunjung === `${clinic.jamBuka}-15.00`
                                    ? "wakun-btn-active"
                                    : ""
                                }`}
                              >
                                {clinic.jamBuka}-15.00 Siang
                              </button>
                              <br />
                              <button
                                value={`15.00-18.00`}
                                onClick={(e) => {
                                  handlerJam(e, "value");
                                }}
                                className={`wakun-btn ${
                                  waktuKunjung === `15.00-18.00`
                                    ? "wakun-btn-active"
                                    : ""
                                }`}
                              >
                                15.00-18.00 Sore{" "}
                              </button>
                            </div>
                            <div className="d-flex">
                              <button
                                value={`18.00-${clinic.jamTutup}`}
                                onClick={(e) => {
                                  handlerJam(e, "value");
                                }}
                                className={`wakun-btn ${
                                  waktuKunjung === `18.00-${clinic.jamTutup}`
                                    ? "wakun-btn-active"
                                    : ""
                                }`}
                              >
                                18.00-{clinic.jamTutup} Malam{" "}
                              </button>
                            </div>
                          </div>
                        )}
                        <h3 className="facil">Fasilitas</h3>
                        <div className="row">
                          <div className="facilities d-flex">
                            <div
                              className={`facility ${
                                clinic.fasilitas.includes("area parkir")
                                  ? "facil-active"
                                  : ""
                              }`}
                            >
                              .
                            </div>
                            <img
                              src={cek}
                              className={`cek-fasil ${
                                clinic.fasilitas.includes("area parkir")
                                  ? "cek-fasil-active"
                                  : ""
                              }`}
                              alt="cek"
                            />
                            <h4>Area Parkir</h4>
                          </div>
                          <div className="facilities d-flex">
                            <div
                              className={`facility ${
                                clinic.fasilitas.includes("ruang tunggu")
                                  ? "facil-active"
                                  : ""
                              }`}
                            >
                              .
                            </div>
                            <img
                              src={cek}
                              className={`cek-fasil ${
                                clinic.fasilitas.includes("ruang tunggu")
                                  ? "cek-fasil-active"
                                  : ""
                              }`}
                              alt="cek"
                            />
                            <h4>Ruang Tunggu</h4>
                          </div>
                        </div>
                        <div className="row">
                          <div className="facilities d-flex">
                            <div
                              className={`facility ${
                                clinic.fasilitas.includes("ruang darurat")
                                  ? "facil-active"
                                  : ""
                              }`}
                            >
                              .
                            </div>
                            <img
                              src={cek}
                              className={`cek-fasil ${
                                clinic.fasilitas.includes("ruang darurat")
                                  ? "cek-fasil-active"
                                  : ""
                              }`}
                              alt="cek"
                            />
                            <h4>Ruang Darurat</h4>
                          </div>
                          <div className="facilities d-flex">
                            <div
                              className={`facility ${
                                clinic.fasilitas.includes("ruang rawat inap")
                                  ? "facil-active"
                                  : ""
                              }`}
                            >
                              .
                            </div>
                            <img
                              src={cek}
                              className={`cek-fasil ${
                                clinic.fasilitas.includes("ruang rawat inap")
                                  ? "cek-fasil-active"
                                  : ""
                              }`}
                              alt="cek"
                            />
                            <h4>Ruang Rawat Inap</h4>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="pilih-dokter">
                      <h2>Pilih Dokter</h2>
                      <div className="row">
                        {doctors
                          .filter((doctor) => clinic.dokter.includes(doctor.id))
                          .map((doctor) => (
                            <div className="col-4">
                              <div
                                className={`d-flex kartudok ${
                                  doctor.jamMulai <= jamMulaiDok &&
                                  doctor.jamPulang >= jamPulangDok
                                    ? "kartudok-active"
                                    : ""
                                } `}
                              >
                                <img src={profilDoc} alt="wow   " />
                                <div>
                                  <h3>Dokter Hewan</h3>
                                  <h1>{doctor.name}</h1>
                                  <div
                                    className={`statuses d-flex ${
                                      doctor.jamMulai <= jamMulaiDok &&
                                      doctor.jamPulang >= jamPulangDok
                                        ? "statuses-active"
                                        : ""
                                    }`}
                                  >
                                    <div
                                      className={`status ${
                                        doctor.jamMulai <= jamMulaiDok &&
                                        doctor.jamPulang >= jamPulangDok
                                          ? "status-active"
                                          : ""
                                      }`}
                                    >
                                      .
                                    </div>
                                    <h4>
                                      {doctor.jamMulai <= jamMulaiDok &&
                                      doctor.jamPulang >= jamPulangDok
                                        ? "Available"
                                        : "Out of Working Hours"}
                                    </h4>
                                  </div>
                                  <div
                                    className={`cek-bungkus ${
                                      doctor.name === dokter
                                        ? "cek-bungkus-chosen"
                                        : ""
                                    }`}
                                    onClick={(e) => handlerDokter(e, "value")}
                                    id={doctor.name}
                                  >
                                    <img
                                      className="cek"
                                      src={cek}
                                      alt="dua"
                                      onClick={(e) => handlerDokter(e, "value")}
                                      id={doctor.name}
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                          ))}
                      </div>
                    </div>
                    <div className="info-hewan">
                      <h3>Masukkan Informasi Hewan Peliharaan</h3>
                      <div className="d-flex">
                        <div className="pet-add">
                          <div className="modal-pet">
                            <ModalPetInfo />
                            <br />
                          </div>
                          <p>Tambahkan Hewan</p>
                        </div>
                        <div>
                          {" "}
                          {localStorage.getItem("namaHewan1") !== null ? (
                            <div className="pet-add">
                              <div>
                                <img
                                  id="1"
                                  onClick={(e) => handlerDelete(e, "id")}
                                  className="close"
                                  src={close}
                                  alt="close"
                                />
                                <img className="pesewet" src={anjing} alt="" />
                              </div>
                              <p>
                                {localStorage.getItem("namaHewan1")} /{" "}
                                {localStorage.getItem("gender1")}
                              </p>
                            </div>
                          ) : (
                            <div></div>
                          )}
                        </div>
                        <div>
                          {" "}
                          {localStorage.getItem("namaHewan2") !== null ? (
                            <div className="pet-add">
                              <div>
                                <img
                                  id="2"
                                  onClick={(e) => handlerDelete(e, "id")}
                                  className="close"
                                  src={close}
                                  alt="close"
                                />
                                <img className="pesewet" src={anjing} alt="" />
                              </div>
                              <p>
                                {localStorage.getItem("namaHewan2")} /{" "}
                                {localStorage.getItem("gender2")}
                              </p>
                            </div>
                          ) : (
                            <div></div>
                          )}
                        </div>
                        <div>
                          {" "}
                          {localStorage.getItem("namaHewan3") !== null ? (
                            <div className="pet-add">
                              <div>
                                <img
                                  id="3"
                                  onClick={(e) => handlerDelete(e, "id")}
                                  className="close"
                                  src={close}
                                  alt="close"
                                />
                                <img className="pesewet" src={anjing} alt="" />
                              </div>
                              <p>
                                {localStorage.getItem("namaHewan3")} /{" "}
                                {localStorage.getItem("gender3")}
                              </p>
                            </div>
                          ) : (
                            <div></div>
                          )}
                        </div>
                        <div>
                          {" "}
                          {localStorage.getItem("namaHewan4") !== null ? (
                            <div className="pet-add">
                              <div>
                                <img
                                  id="4"
                                  onClick={(e) => handlerDelete(e, "id")}
                                  className="close"
                                  src={close}
                                  alt="close"
                                />
                                <img className="pesewet" src={anjing} alt="" />
                              </div>
                              <p>
                                {localStorage.getItem("namaHewan4")} /{" "}
                                {localStorage.getItem("gender4")}
                              </p>
                            </div>
                          ) : (
                            <div> </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="d-flex justify-content-center">
                    <button
                      className="book-now"
                      onClick={(e) => history.push("/book-resume")}
                    >
                      Book Now
                    </button>
                  </div>
                  <br />
                  <br />
                  <br />
                </div>
              </div>
              <Footer />
            </div>
          ))
      )}
    </div>
  );
};

export default BookingDetails;
