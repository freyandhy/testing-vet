import Axios from "axios";
import React, { useState, useEffect } from "react";
import {
  Row,
  Label,
  Form,
  FormGroup,
  Input,
  Button,
  ModalBody,
  Modal,
} from "reactstrap";

const EditDoctorRight = () => {
  const checklist =
    "https://ik.imagekit.io/zvqf9loqe2/VET/Group_DcxThd9dOuTC.svg";
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);

  const [doctor, setDoctor] = useState([]);

  useEffect(() => {
    Axios.get("https://5fbcb4963f8f90001638c271.mockapi.io/dokter")
      .then((res) => {
        setDoctor(res.data[0]);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <div className="rightSide">
      <Form>
        <div>
          <h3 className="form">Upload Photo</h3>
          <hr />
          <br />
          <div className="rcorners">
            <div className="upload">
              <img src="https://ik.imagekit.io/zvqf9loqe2/VET/Vector__1__wVzNtBzycat.svg" />
            </div>
            <div className="upload-text">
              <h5 style={{ color: "#818181" }}>Upload Image</h5>
            </div>
          </div>

          <h3 className="form">Doctor Information</h3>
          <hr />

          <Label className="form">Status</Label>
          <br />
          <Row className="radioButton">
            {doctor.status === true ? (
              <FormGroup check>
                <Label
                  style={{ marginLeft: "-34px", marginRight: "80px" }}
                  check
                >
                  <Input type="radio" name="radio1" checked />
                  Active
                </Label>

                <Label check>
                  <Input type="radio" name="radio1" />
                  Offline
                </Label>
              </FormGroup>
            ) : (
              <FormGroup check>
                <Label
                  style={{ marginLeft: "-34px", marginRight: "80px" }}
                  check
                >
                  <Input type="radio" name="radio1" />
                  Active
                </Label>

                <Label check>
                  <Input type="radio" name="radio1" checked />
                  Offline
                </Label>
              </FormGroup>
            )}
          </Row>

          <br />

          <Row>
            <div className="d-flex" style={{ marginLeft: "3rem" }}>
              <FormGroup>
                <Label for="exampleSelect">Jam Mulai</Label>
                <Input
                  // style={{ width: "200px" }}
                  type="select"
                  name="select"
                  id="exampleSelect"
                >
                  <option>{doctor.jamMulai}</option>
                  <option>08.00</option>
                  <option>09.00</option>
                  <option>10.00</option>
                  <option>11.00</option>
                  <option>12.00</option>
                  <option>13.00</option>
                  <option>14.00</option>
                  <option>15.00</option>
                </Input>
              </FormGroup>

              <FormGroup className="form">
                <Label for="exampleSelect">Jam Pulang</Label>
                <Input
                  // style={{ width: "200px" }}
                  type="select"
                  name="select"
                  id="exampleSelect"
                >
                  <option>{doctor.jamPulang}</option>
                  <option>11.00</option>
                  <option>12.00</option>
                  <option>13.00</option>
                  <option>14.00</option>
                  <option>15.00</option>
                  <option>16.00</option>
                  <option>17.00</option>
                  <option>18.00</option>
                  <option>19.00</option>
                  <option>20.00</option>
                  <option>21.00</option>
                </Input>
              </FormGroup>
            </div>
          </Row>

          <br />
          <br />
          <h3 className="form">Basic Information</h3>
          <hr />

          <Label className="form">Nama Lengkap</Label>
          <br />
          <div className="form1">
            <Input
              className="input"
              type="text"
              placeholder="Alexandria"
              defaultValue={doctor.name}
            />
          </div>

          <br />
          <Label className="form">Gender</Label>
          <br />
          <Row className="radioButton">
            {doctor.gender === true ? (
              <FormGroup check>
                <Label
                  style={{ marginLeft: "-34px", marginRight: "80px" }}
                  check
                >
                  <Input type="radio" name="radio1" checked />
                  Male
                </Label>

                <Label check>
                  <Input type="radio" name="radio1" />
                  Female
                </Label>
              </FormGroup>
            ) : (
              <FormGroup check>
                <Label
                  style={{ marginLeft: "-34px", marginRight: "80px" }}
                  check
                >
                  <Input type="radio" name="radio1" />
                  Male
                </Label>

                <Label check>
                  <Input type="radio" name="radio1" checked />
                  Female
                </Label>
              </FormGroup>
            )}
          </Row>

          <br />

          <Label className="form">Experience</Label>
          <br />
          <div className="form1">
            <Input
              className="input"
              type="text"
              placeholder="12 Tahun"
              defaultValue={doctor.pengalaman}
            />
          </div>

          <br />
          <br />
          <h3 className="form">Contact Details</h3>
          <hr />

          <Label className="form">Nomor Telephone</Label>
          <br />
          <div className="form1">
            <Input
              className="input"
              type="telephone"
              placeholder="+712371723 18231823"
              defaultValue={doctor.telepon}
            />
          </div>

          <br />
          <Label className="form">Email</Label>
          <br />
          <div className="form1">
            <Input
              className="input"
              type="email"
              placeholder="alexanda@gmail.com"
              defaultValue={doctor.email}
            />
          </div>
          <br />
          <br />
          <br />
        </div>
      </Form>

      <div>
        <Button
          className="tombol-simpan"
          type="simpan"
          color="warning"
          onClick={toggle}
        >
          Simpan
        </Button>
        <Modal
          isOpen={modal}
          toggle={toggle}
          style={{ marginTop: "13rem", borderRadius: "25px" }}
        >
          <ModalBody
            style={{
              backgroundColor: "#1A3150",
              paddingTop: "3rem",
              paddingBottom: "3rem",
              borderRadius: "2px",
            }}
          >
            <div className="col ">
              <div className="row d-flex justify-content-center">
                <img src={checklist} />
              </div>

              <br />
              <div className="row d-flex justify-content-center">
                <h1 style={{ color: "white" }}>Update Sukses</h1>
              </div>
            </div>
          </ModalBody>
        </Modal>
      </div>

      <br />
      <br />
    </div>
  );
};

export default EditDoctorRight;
