import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Container, Row, Col, CardText, CardBody } from 'reactstrap';
import './AppointmentClinic.css';

const AppointmentClinic = () => {

  useEffect(()=>{
    Axios.get('')
  },[])
  return (
    <>
      <Container>
        <div tabIndex="1" className="card w-100 appointment-card mb-2">
          <CardBody>
            <CardText>
              <Row className="justify-content-around text-center">
                <Col>
                  <div className="appointment-date d-flex align-items-center">
                    12 OKT
                  </div>
                </Col>

                <Col className="d-flex align-items-center justify-content-center">
                  <div className="name-of-clinic">drh. Ivan Adrianto</div>
                </Col>
                <Col className="d-flex align-items-center justify-content-center">
                  <div className="clinic-name">Ivan Kasella</div>
                </Col>
                <Col className="d-flex align-items-center justify-content-center">
                  <div className="appointment-time">19.20</div>
                </Col>
                <Col
                  sm="3"
                  className="d-flex align-items-center justify-content-center">
                  <span>
                    <img src="https://ik.imagekit.io/zvqf9loqe2/VET/eva_message-square-fill_BKxk6qNCntU.svg" />
                  </span>
                </Col>
              </Row>
            </CardText>
          </CardBody>
        </div>
        <div tabIndex="1" className="card w-100 appointment-card mb-2">
          <CardBody>
            <CardText>
              <Row className="justify-content-around text-center">
                <Col>
                  <div className="appointment-date d-flex align-items-center">
                    12 OKT
                  </div>
                </Col>

                <Col className="d-flex align-items-center justify-content-center">
                  <div className="name-of-clinic">drh. Ivan Adrianto</div>
                </Col>
                <Col className="d-flex align-items-center justify-content-center">
                  <div className="clinic-name">Ivan Kasella</div>
                </Col>
                <Col className="d-flex align-items-center justify-content-center">
                  <div className="appointment-time">19.20</div>
                </Col>
                <Col
                  sm="3"
                  className="d-flex align-items-center justify-content-center">
                  <span>
                    <img src="https://ik.imagekit.io/zvqf9loqe2/VET/eva_message-square-fill_BKxk6qNCntU.svg" />
                  </span>
                </Col>
              </Row>
            </CardText>
          </CardBody>
        </div>
        <div tabIndex="1" className="card w-100 appointment-card mb-2">
          <CardBody>
            <CardText>
              <Row className="justify-content-around text-center">
                <Col>
                  <div className="appointment-date d-flex align-items-center">
                    12 OKT
                  </div>
                </Col>

                <Col className="d-flex align-items-center justify-content-center">
                  <div className="name-of-clinic">drh. Ivan Adrianto</div>
                </Col>
                <Col className="d-flex align-items-center justify-content-center">
                  <div className="clinic-name">Ivan Kasella</div>
                </Col>
                <Col className="d-flex align-items-center justify-content-center">
                  <div className="appointment-time">19.20</div>
                </Col>
                <Col
                  sm="3"
                  className="d-flex align-items-center justify-content-center">
                  <span>
                    <img src="https://ik.imagekit.io/zvqf9loqe2/VET/eva_message-square-fill_BKxk6qNCntU.svg" />
                  </span>
                </Col>
              </Row>
            </CardText>
          </CardBody>
        </div>
        <div tabIndex="1" className="card w-100 appointment-card mb-2">
          <CardBody>
            <CardText>
              <Row className="justify-content-around text-center">
                <Col>
                  <div className="appointment-date d-flex align-items-center">
                    12 OKT
                  </div>
                </Col>

                <Col className="d-flex align-items-center justify-content-center">
                  <div className="name-of-clinic">drh. Ivan Adrianto</div>
                </Col>
                <Col className="d-flex align-items-center justify-content-center">
                  <div className="clinic-name">Ivan Kasella</div>
                </Col>
                <Col className="d-flex align-items-center justify-content-center">
                  <div className="appointment-time">19.20</div>
                </Col>
                <Col
                  sm="3"
                  className="d-flex align-items-center justify-content-center">
                  <span>
                    <img src="https://ik.imagekit.io/zvqf9loqe2/VET/eva_message-square-fill_BKxk6qNCntU.svg" />
                  </span>
                </Col>
              </Row>
            </CardText>
          </CardBody>
        </div>
      </Container>
    </>
  );
};

export default AppointmentClinic;
