import React, { useState, useEffect } from "react";
import Header from "../layout/Header";
import Carousel from "react-elastic-carousel";
import "./Home.css";
import { Link } from "react-router-dom";

import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardText,
  // CardGroup,
} from "reactstrap";
import dogImage from "./image/home-dog.png";
import catImage from "./image/home-cat.png";
import serviceLogoMid from "./image/home-service-logo-mid.svg";
import serviceLogoLeft from "./image/home-service-logo-left.svg";
import serviceLogoRight from "./image/home-service-logo-right.svg";
import axios from "axios";

const Home = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [pages, setPages] = useState(1);
  const [limits] = useState(3);
  const [totalPages, setTotalPages] = useState(1);
  const [clinics, setClinics] = useState([]);

  useEffect(() => {
    setIsLoading(true);
    const url = `https://5fbcb4963f8f90001638c271.mockapi.io/search`;
    axios.get(url).then((res) => {
      // console.log(res.data);
      setClinics(res.data);
      setTotalPages(5);
      setIsLoading(false);
    });
  }, []);

  // console.log(clinics);

  const slides = clinics.map((item) => {
    return (
      <Col key={item.id}>
        <Card style={{ margin: "auto", borderRadius: "11px" }}>
          <CardImg src={item.poster} width="200px" height="150px" />
          <CardBody>
            <button className="btn-loc">{item.kota}</button>
            <CardTitle style={{ height: "3em", overflow: "hidden" }}>
              {item.namaKlinik}
            </CardTitle>
            <CardText>{item.Year}</CardText>
            <a href="/klinik" className="btn btn-block">
              find more
            </a>
          </CardBody>
        </Card>
      </Col>
    );
  });

  return (
    <>
      <Header />
      <div className="home-top">
        <div className="home-rectangle">
          <img src={dogImage} alt="dog" className="home-dog" />
        </div>
        <div className="home-elipse">
          <Container fluid={true}>
            <div className="ml-5 jumbotron px-0 pt-0">
              <div className="col-md-6 px-0">
                <h1>Welcome to VET</h1>
                <p className="mt-3">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt
                  laboriosam natus eius architecto debitis dolorem odit illo
                  voluptatibus magni vitae, dolor sapiente nulla laudantium
                  voluptatum beatae earum ad. Quod, vero!
                </p>
                {/* <button className="btn btn-warning mt-3 py-3 px-5"> */}
                <Link
                  to="/klinik"
                  className="booknow-text btn btn-warning mt-3 py-3 px-5"
                >
                  Booking Now
                </Link>
                {/* <span className="booknow-text" onClick={manageBookNow}>
                    Booking Now
                  </span> */}
                {/* </button> */}
              </div>
            </div>
          </Container>
        </div>
      </div>
      <div className="services">OUR SERVICES</div>
      <Container fluid={true}>
        <Row className="align-items-center justify-content-around">
          <Col sm="3" className="text-center">
            <img src={serviceLogoLeft} alt="logoleft" />
            <h4>Klinik/Rumah Sakit</h4>
            <p>Pilih Klinik atau rumah saki yang terdekat dengan anda</p>
          </Col>
          <Col sm="3" className="text-center">
            <div className="services-logo">
              <img src={serviceLogoMid} alt="logomid" />
            </div>
            <h4>Tanggal Konsultasi</h4>
            <p>Tentukan tanggal beserta jam konsultasi dengan dokter hewan</p>
          </Col>
          <Col sm="3" className="text-center">
            <div className="services-logo">
              <img src={serviceLogoRight} alt="logoright" />
            </div>
            <h4>Bertemu Dokter</h4>
            <p>Waktunya bertemu dengan dokter untuk berkonsultasi</p>
          </Col>
        </Row>
        <Row className="align-items-center mx-5 px-1 text-middle">
          <Col sm="6">
            <div className="cat-bg-img text-center">
              <img src={catImage} alt="catimage" />
            </div>
          </Col>
          <Col>
            <h2>Why Choose Us?</h2>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Dignissimos nisi, perspiciatis explicabo esse officiis inventore
              ad, accusantium assumenda et in tempore similique doloribus magnam
              sit error nesciunt fugit quis hic.
            </p>
          </Col>
        </Row>

        <Row className="align-items-center justify-content-between mx-5 px-1 text-middle">
          <Col sm="5" className="mr-5">
            <h2>Pilih Klinik</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto,
              placeat explicabo pariatur praesentium officia minima aut alias
              dignissimos temporibus molestias quaerat vero qui dolorum sequi
              velit asperiores! Ea voluptate, mollitia excepturi perferendis,
              cumque et quaerat blanditiis nobis aut in vero.
            </p>
          </Col>
          <Col className="ml-auto justify-items-center">
            {isLoading === true ? (
              <div>
                <div className="spinner">
                  <div className="bounce1"></div>
                  <div className="bounce2"></div>
                  <div className="bounce3"></div>
                </div>
              </div>
            ) : (
              <Carousel itemsToShow={2}>{slides}</Carousel>
            )}
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Home;
