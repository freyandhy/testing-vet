import React, { useState, useEffect } from "react";
import { Container, Row, Col, CardText, CardBody } from "reactstrap";
import "./UserApp.css";
import axios from "axios";

const UserApp = () => {
  const [isLoading, setIsLoading] = useState([]);
  const [appointmentLists, setAppointmentLists] = useState([]);
  const [clinicLists, setClinicLists] = useState([]);
  const [chosenClinicId, setChosenClinicId] = useState("");
  const [chosenClinic, setChosenClinic] = useState([]);

  useEffect(() => {
    const appointmentListsUrl =
      "https://5fbcb4963f8f90001638c271.mockapi.io/appointments";
    const clinicListsUrl = `https://5fbcb4963f8f90001638c271.mockapi.io/search`;
    axios
      .get(appointmentListsUrl)
      .then((res) => setAppointmentLists(res.data))
      .then(axios.get(clinicListsUrl).then((res) => setClinicLists(res.data)));
    // .then(
    //   axios
    //     .get(
    //       `https://5fbcb4963f8f90001638c271.mockapi.io/search/${chosenClinicId}`
    //     )
    //     .then((res) => console.log(res.data))
    // );
  }, []);

  // console.log(appointmentLists);
  // console.log(clinicLists);

  return (
    <>
      <Container>
        {isLoading === true ? (
          <div>
            <div className="spinner">
              <div className="bounce1"></div>
              <div className="bounce2"></div>
              <div className="bounce3"></div>
            </div>
          </div>
        ) : (
          appointmentLists.map((appointmentList) => {
            return (
              <div
                tabIndex="1"
                className="card w-100 appointment-card mb-2"
                key={appointmentList.id}
              >
                <CardBody>
                  <Row className="justify-content-around text-center">
                    <Col>
                      <div className="appointment-date d-flex align-items-center">
                        {appointmentList.tgl}
                      </div>
                    </Col>

                    <Col className="d-flex align-items-center justify-content-center">
                      <div className="name-of-clinic">
                        {appointmentList.namaKlinik}
                      </div>
                    </Col>
                    <Col className="d-flex align-items-center justify-content-center">
                      <div className="appointment-time">
                        {appointmentList.jam}
                      </div>
                    </Col>
                    <Col
                      sm="3"
                      className="d-flex align-items-center justify-content-center"
                    >
                      {appointmentList.status ? (
                        <span
                          className="btn btn-warning"
                          style={{
                            borderRadius: "25px",
                            backgroundColor: "lightgreen",
                            border: "2px solid #73AD21",
                          }}
                        >
                          Approve
                        </span>
                      ) : (
                        <span
                          className="btn btn-danger"
                          style={{
                            borderRadius: "25px",
                            backgroundColor: "pink",
                            border: "2px solid #F15A61",
                          }}
                        >
                          Disapprove
                        </span>
                      )}
                    </Col>
                  </Row>
                </CardBody>
              </div>
            );
          })
        )}
      </Container>
    </>
  );
};

export default UserApp;
