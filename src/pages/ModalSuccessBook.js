import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import './modalSuccessBook.css'
import mantap from './../img/Vector (3).png'

const ModalExample = (props) => {
    const {
        buttonLabel,
        className
    } = props;

    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    const handlerClick = () => {

        const urlBooking = "https://5fbcb4963f8f90001638c271.mockapi.io/bookings"
        const bodyDataBook = {
            tgl: localStorage.getItem('tglKunjungan'),
            jam: localStorage.getItem('waktuKunjungan').slice(0, 5),
            klinikId: localStorage.getItem('klinikId'),
            pets: localStorage.getItem('namaHewan2') === null ? '1' : localStorage.getItem('namaHewan3') === null ? '2' : localStorage.getItem('namaHewan3') === null ? '3' : '4'
        }   

        const urlAppointment = "https://5fbcb4963f8f90001638c271.mockapi.io/appointments"
        const bodyDataApp = {
            tgl: localStorage.getItem('tglKunjungan'),
            jam: localStorage.getItem('waktuKunjungan').slice(0, 5),
            klinikId: localStorage.getItem('klinikId'),
        }

        fetch(urlBooking, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            }, body: JSON.stringify(bodyDataBook)
        })
            .then(() => {
                toggle()
                localStorage.removeItem('namaHewan1')
                localStorage.removeItem('namaHewan2')
                localStorage.removeItem('namaHewan3')
                localStorage.removeItem('namaHewan4')
                localStorage.removeItem('namaDokter')
                localStorage.removeItem('waktuKunjungan')
                localStorage.removeItem('hariKunjungan')
            })

        fetch(urlAppointment, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            }, body: JSON.stringify(bodyDataApp)
        })

    }

    return (
        <div className='modal-success'>
            <button className='finish-booking' onClick={handlerClick}>Selesai</button>
            <Modal isOpen={modal} toggle={toggle} className={className}>
                <ModalBody className='wow'>
                    <img src={mantap} alt="mantap" /><br />
                    <h1>Booking Success</h1>
                </ModalBody>
            </Modal>
        </div>
    );
}

export default ModalExample;