import React, { useState, useEffect } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardText,
  CardBody,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import "./DoctorApproval.css";
import axios from "axios";

const DoctorApproval = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [modal, setModal] = useState(false);
  const [approvalLists, setApprovalLists] = useState([]);
  const toggle = () => setModal(!modal);

  useEffect(() => {
    setIsLoading(true);
    const url = "https://5fbcb4963f8f90001638c271.mockapi.io/bookings";
    axios.get(url).then((res) => {
      setApprovalLists(res.data);
      setIsLoading(false);
    });
  }, []);

  return (
    <Container>
      {isLoading ? (
        <div>
          <div className="spinner">
            <div className="bounce1"></div>
            <div className="bounce2"></div>
            <div className="bounce3"></div>
          </div>
        </div>
      ) : (
        approvalLists.map((approvalList) => {
          // console.log(approvalList);
          return (
            <div
              tabIndex="1"
              className="card w-100 doctor-approval-card mb-2"
              key={approvalList.id}
            >
              <CardBody>
                <Row className="justify-content-around text-center">
                  <Col>
                    <div className="approval-date d-flex align-items-center justify-content-center">
                      {approvalList.tgl}
                    </div>
                  </Col>
                  <Col sm="2" className="d-flex align-items-center">
                    <div className="approval-name">{approvalList.user}</div>
                  </Col>
                  <Col className="d-flex align-items-center justify-content-center">
                    <div className="approval-pet-counts">
                      {approvalList.pets} pets
                    </div>
                  </Col>
                  <Col className="d-flex align-items-center justify-content-center">
                    <div className="approval-time">{approvalList.jam}</div>
                  </Col>
                  <Col
                    sm="3"
                    className="d-flex align-items-center justify-content-center"
                  >
                    <span className="btn btn-warning" onClick={toggle}>
                      {approvalList.status}
                    </span>
                  </Col>
                </Row>
              </CardBody>
            </div>
          );
        })
      )}

      <Modal isOpen={modal} toggle={toggle} className="doctor-modal-placement">
        <ModalBody className="text-center">
          Are you sure want to finish the appointment?
        </ModalBody>
        <ModalFooter className="justify-content-center">
          <Button color="light" onClick={toggle} className="px-5 mx-3">
            Cancel
          </Button>
          <Button color="warning" onClick={toggle} className="px-5 mx-3">
            Finish
          </Button>
        </ModalFooter>
      </Modal>
    </Container>
  );
};

export default DoctorApproval;
