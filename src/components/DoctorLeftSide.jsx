import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import { NavLink, useRouteMatch } from "react-router-dom";

const DoctorLeftSide = (props) => {
  let { path, url } = useRouteMatch();

  return (
    <>
      <div className="leftSide text-center mx-5">
        <div>
          <img
            src="https://ik.imagekit.io/zvqf9loqe2/VET/Ellipse_10_tm2bFcdD2lB.svg"
            width="30%"
            className="mt-4"
          />
          <div className="profile-name">
            <h3>Alexandria Raihan</h3>
            <div className="container">
              <div className="statusDoc btn doctor-btn align-items-center">{`DOCTOR`}</div>
            </div>
          </div>
        </div>

        <Row className="profile1-comp mt-4">
          <Col>
            <img
              src="https://ik.imagekit.io/zvqf9loqe2/VET/Vector__2___vbNttX3HpvA.svg"
              width="18%"
            ></img>
            <h5 style={{ color: "green" }}>Online</h5>
          </Col>
          <Col>
            <img
              src="https://ik.imagekit.io/zvqf9loqe2/VET/Vector__3__O8AHPr8lggMB.svg"
              width="18%"
            ></img>
            <h5>3 Years</h5>
          </Col>
        </Row>

        <hr style={{ marginRight: "50px", marginLeft: "50px" }} />
        <NavLink
          to={`${path}/edit-profile`}
          exact={true}
          className="nav-link inactive-nav-edit-profile"
          activeClassName="active-nav-edit-profile"
          onClick={props.handleEditProfile}
        >
          Edit Profile
        </NavLink>
        <br />
      </div>

      <div className="logout mx-5 mb-5">
        <Row className="logout-comp">
          <img
            src="https://ik.imagekit.io/zvqf9loqe2/VET/clarity_logout-line_6JOgslcDRtq.svg"
            className="ml-5"
          />
          <div className="text">
            <h5>{`Logout`}</h5>
          </div>
        </Row>
      </div>
    </>
  );
};

export default DoctorLeftSide;
