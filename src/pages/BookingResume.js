import Navbar from "./../layout/Header";
import "./bookingResume.css";
import gambar from "./../img/image 4 (1).png";
import ModalSuccessBook from "./ModalSuccessBook";

const BookingResume = () => {
    return (
        <div>
            <Navbar /> <br /> <br /> <br />
            <div className="container resume-booking">
                <div className='resume-title'>
                    <h1>Resume Booking</h1>
                    <hr />
                    <h4>{localStorage.getItem('namaRS')}</h4>
                </div>
                <div className="row">
                    <div className="col-lg-6 col-sm-12 col-md-12">
                        <img src={gambar} alt="gambar" />
                    </div>
                    <div className="col-lg-6 col-sm-12 col-md-12">
                        <h2>Informasi Kunjungan</h2>
                        <div className="info-final">
                            <h5>Hari &amp; Waktu Kunjungan </h5>
                            <p>{localStorage.getItem('hariKunjungan') === null ? 'Pilih hari dan waktu kunjungan terlebih dahulu' : `${localStorage.getItem('hariKunjungan')} ${localStorage.getItem('tglKunjungan')}, pukul ${localStorage.getItem('waktuKunjungan')}`} </p>
                            <h5>Dokter</h5>
                            <p>{localStorage.getItem('namaDokter') ? localStorage.getItem('namaDokter')  : 'Pilih dokter terlebih dahulu'}</p>
                            <h5>Hewan Peliharaan</h5>
                            <p>{localStorage.getItem('namaHewan1') !== null ? `1. ${localStorage.getItem('namaHewan1')}, ${localStorage.getItem('gender1')}` : ''}</p>
                            <p>{localStorage.getItem('namaHewan2') !== null ? `2. ${localStorage.getItem('namaHewan2')}, ${localStorage.getItem('gender2')}` : ''}</p>
                            <p>{localStorage.getItem('namaHewan3') !== null ? `3. ${localStorage.getItem('namaHewan3')}, ${localStorage.getItem('gender3')}` : ''}</p>
                            <p>{localStorage.getItem('namaHewan4') !== null ? `4. ${localStorage.getItem('namaHewan4')}, ${localStorage.getItem('gender4')}` : ''}</p>
                        </div>
                    </div>
                </div>
                <hr />
                <ModalSuccessBook />
            </div>
        </div>
  );
};

export default BookingResume;
