import React, { useState } from 'react';
import {
    Card,
    CardBody,
    Input,
    FormGroup,
    Button,
    Form,
    Nav,
    NavLink,
    Navbar,
    NavItem,
    Collapse,
    NavbarToggler
} from 'reactstrap';
import axios from 'axios';
import { useHistory, Link } from 'react-router-dom';
import './Register.css'
import anjing from './../image-dog/ANJING.png'

const Register = () => {
    let history = useHistory();

    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);

    const [fullName, setFullName] = useState();
    const [userName, setUserName] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [ isWrongRegister, setIsWrongRegister ] = useState(false);

    const handleRegister = (e) => {
        e.preventDefault();

        let url = 'http://13.236.9.153/auth/register'
        const data = {
            fullname: fullName,
            username: userName,
            email: email,
            password: password,
            role: localStorage.getItem('role'),
        };

        if (localStorage.getItem('role') === 'admin') {
            url = 'http://13.236.9.153/auth/admin/register'
        } else if (localStorage.getItem('role') === 'user') {
            url = 'http://13.236.9.153/auth/register'
        }

        axios.post(url, data).then((ress) => {
            console.log(ress.data);
            if (localStorage.getItem('role') === 'user') {
                localStorage.setItem('id', ress.data.id)
                localStorage.setItem('username', ress.data.username)
                localStorage.setItem('fullname', ress.data.fullname)
                localStorage.setItem('email', ress.data.email)
                localStorage.setItem('password', ress.data.password)
                localStorage.setItem('phoneNumber', ress.data.phoneNumber)
                localStorage.setItem('gender', ress.data.gender)
                localStorage.setItem('pictureurl', ress.data.pictureUrl)
                localStorage.setItem('role', ress.data.role)
                localStorage.setItem('clinicid', ress.data.clinicId)
                localStorage.setItem('token', ress.data.token)
                history.push('/')
            } else if (localStorage.getItem('role') === 'admin') {
                localStorage.setItem('id', ress.data.userAdmins.id)
                localStorage.setItem('username', ress.data.user.username)
                localStorage.setItem('fullname', ress.data.user.fullname)
                localStorage.setItem('email', ress.data.user.email)
                localStorage.setItem('password', ress.data.user.password)
                localStorage.setItem('phoneNumber', ress.data.user.phoneNumber)
                localStorage.setItem('gender', ress.data.user.gender)
                localStorage.setItem('pictureurl', ress.data.user.pictureUrl)
                localStorage.setItem('role', ress.data.user.role)
                localStorage.setItem('clinicid', ress.data.user.clinicId)
                localStorage.setItem('token', ress.data.user.token)
                history.push('/managedoctor/edit-profile')
            } else {
                history.push('/managedoctor/edit-profile')
            }
        })
        .catch (
            () => {setIsWrongRegister(true)}
        )
    };

    return (
        <div>
            <div className="regist-header">
                <Navbar light expand="md" className="pt-0">
                    <NavbarToggler onClick={toggle} className="ml-auto" />
                    <Collapse isOpen={isOpen} navbar>
                        <Nav className="ml-auto font-weight-bold" navbar>
                            <NavItem>
                                <NavLink href="/" className="regist-text ml-2">
                                    Home
                                    </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/dokter" className="regist-text ml-2">
                                    Doctor
                                    </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/klinik" className="regist-tex ml-2">
                                    Search Clinic
                                    </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    href="/login"
                                    className="btn btn-warning border-0 pr-3 pl-3 ml-2"
                                    style={{ backgroundColor: "#FDCB5A", color: "#1A3150" }}>
                                    Login
                                </NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
            <div className="vh-100 d-flex align-items-center justify-content-center">
                <div className="dog">
                    <img src={anjing}
                        alt="dog-icon"
                        style={{ width: '90%' }}>
                    </img>
                    <img src='https://ik.imagekit.io/tk1apipynn/final-assets/Group_6_vqzZkWHlEfjD.svg'
                        alt=''
                        className="vet"></img>
                </div>
                <Card className="box-regist" style={{ width: '50%', borderColor: "#FFFFFF" }}>
                    <CardBody>
                        <h2 className="mb-4">Make a New Account</h2>
                        <h6 className="text-regist">Register Yourself To Use Our Application</h6>
                        <Form onSubmit={handleRegister}>
                            <FormGroup>
                                <Input
                                    type="text"
                                    name="fullname"
                                    onChange={(e) => setFullName(e.target.value)}
                                    placeholder="Full Name"
                                />
                                <img
                                    src="https://ik.imagekit.io/tk1apipynn/final-assets/username_BBdsAt8mIxd.svg"
                                    alt=""
                                    className="reg-icon"
                                />
                            </FormGroup>
                            <FormGroup>
                                <Input
                                    type="text"
                                    name="username"
                                    onChange={(e) => setUserName(e.target.value)}
                                    placeholder="User Name"
                                />
                                <img
                                    src="https://ik.imagekit.io/tk1apipynn/final-assets/username_BBdsAt8mIxd.svg"
                                    alt=""
                                    className="reg-icon"
                                />
                            </FormGroup>
                            <FormGroup>
                                <Input
                                    type="email"
                                    name="email"
                                    onChange={(e) => setEmail(e.target.value)}
                                    placeholder="Email"
                                />
                                <img
                                    src="https://ik.imagekit.io/tk1apipynn/final-assets/Vector_email_PR13Z2PVg3ku.svg"
                                    alt=""
                                    className="reg-icon"
                                />
                            </FormGroup>
                            <FormGroup>
                                <Input
                                    type="password"
                                    name="password"
                                    onChange={(e) => setPassword(e.target.value)}
                                    placeholder="Password"
                                />
                                <img
                                    src="https://ik.imagekit.io/tk1apipynn/final-assets/Vector_password_zGv3zGC5kVG.svg"
                                    alt=""
                                    className="reg-icon"
                                />
                                <img
                                    src="https://ik.imagekit.io/tk1apipynn/final-assets/password_iV1kjFGTvtiP.svg"
                                    alt=""
                                    className="pw-icon"
                                />
                            </FormGroup>
                            {isWrongRegister === true ? <h6 className="error-regist">The username is does exist, please use another one.</h6> : ''}
                            <Button className="btn btn-block">Register</Button>
                            <h6 className="text-regist">Already have an account? Please
                            <Link to="/login" className="signin"> Sign In</Link>
                            </h6>
                        </Form>
                    </CardBody>
                </Card>

            </div>
        </div>
    )

}

export default Register;
