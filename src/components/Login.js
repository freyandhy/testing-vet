import React, { useState } from "react";
import {
  Card,
  CardBody,
  Input,
  FormGroup,
  Button,
  Form,
  Navbar,
  Nav,
  NavItem,
  NavLink,
  Collapse,
  NavbarToggler,
} from "reactstrap";
import axios from "axios";
import { useHistory, Link } from "react-router-dom";
import Cookies from "js-cookie";
import { checkLogin } from "../Helper";
import "../components/Login.css";
import anjing from "./../image-dog/ANJING.png";

const Login = () => {
  let history = useHistory();

  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [ isWrongLogin, setIsWrongLogin ] = useState(false);

  const handleLogin = (e) => {
    e.preventDefault();

    const url = "http://13.236.9.153/auth/login";
    const data = {
      credential: username,
      password: password,
    };

    axios.post(url, data).then((ress) => {
      console.log(ress.data);
      // history.push("/");
      localStorage.setItem("id", ress.data.id);
      localStorage.setItem("username", ress.data.username);
      localStorage.setItem("fullname", ress.data.fullname);
      localStorage.setItem("email", ress.data.email);
      localStorage.setItem("password", ress.data.password);
      localStorage.setItem("phoneNumber", ress.data.phoneNumber);
      localStorage.setItem("gender", ress.data.gender);
      localStorage.setItem("pictureurl", ress.data.pictureUrl);
      localStorage.setItem("role", ress.data.role);
      localStorage.setItem("clinicid", ress.data.clinicId);
      localStorage.setItem("token", ress.data.token);
      if (localStorage.getItem("role") === "user") {
        history.push("/userhistory");
      } else if (localStorage.getItem("role") === "admin") {
        history.push("/managedoctor");
      } else {
        history.push("/dokter");
      }
    })
    .catch(
      () => {setIsWrongLogin(true)}
    )
  };
  
  const handleLogout = () => {
    Cookies.remove("token");
    history.push("/");
  };

  const renderBtn = () => {
    if (!checkLogin()) {
      return (
        <div>
          <div className="login-header">
            <Navbar light expand="md" className="pt-0">
              <NavbarToggler onClick={toggle} className="ml-auto" />
              <Collapse isOpen={isOpen} navbar>
                <Nav className="ml-auto font-weight-bold" navbar>
                  <NavItem>
                    <NavLink href="/" className="login-text ml-2">
                      Home
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href="/dokter" className="login-text ml-2">
                      Doctor
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href="/klinik" className="login-tex ml-2">
                      Search Clinic
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="/register"
                      className="btn btn-warning border-0 pr-3 pl-3 ml-2"
                    >
                      Registration
                    </NavLink>
                  </NavItem>
                </Nav>
              </Collapse>
            </Navbar>
          </div>
          <div className="vh-100 d-flex align-items-center justify-content-center">
            <div className="dog">
              <img src={anjing} alt="dog-icon" width="88%"></img>
              <img
                src="https://ik.imagekit.io/tk1apipynn/final-assets/Group_6_vqzZkWHlEfjD.svg"
                alt=""
                className="vet"
              ></img>
            </div>

            <Card
              className="box-login"
              style={{ width: "50%", borderColor: "#FFFFFF" }}
            >
              <CardBody>
                <h2 className="mb-4">Welcome Back</h2>
                <h6 className="text-login">Login Here to Start Your Journey</h6>
                <Form onSubmit={handleLogin}>
                  <FormGroup>
                    <Input
                      type="text"
                      name="username"
                      background-color="#FFFFFF"
                      onChange={(e) => setUserName(e.target.value)}
                      placeholder="User Name"
                    />
                    <img
                      src="https://ik.imagekit.io/tk1apipynn/final-assets/Vector_email_PR13Z2PVg3ku.svg"
                      alt=""
                      className="username"
                    />
                  </FormGroup>
                  <FormGroup>
                    <Input
                      type="password"
                      name="password"
                      background-color="#FFFFFF"
                      onChange={(e) => setPassword(e.target.value)}
                      placeholder="Password"
                    />
                    <img
                      src="https://ik.imagekit.io/tk1apipynn/final-assets/Vector_password_zGv3zGC5kVG.svg"
                      alt=""
                      className="username"
                    />
                    <img
                      src="https://ik.imagekit.io/tk1apipynn/final-assets/password_iV1kjFGTvtiP.svg"
                      alt=""
                      className="pw-icon"
                    />
                  </FormGroup>
                  {isWrongLogin === true ? <h6 className="error-text">Your username or password is wrong, please try again...</h6> : '' } 
                  <Button className="btn btn-block" background-color="#FDCB5A">
                    Login
                  </Button>
                  <h6 className="text-login2">
                    Don't have an account? Please
                    <Link to="/register" className="signup">
                      {" "}
                      Sign Up
                    </Link>
                  </h6>
                </Form>
              </CardBody>
            </Card>
          </div>
        </div>
      );
    } else {
      return (
        <button className="btn btn-block" onClick={handleLogout}>
          LOGOUT
        </button>
      );
    }
  };
  return <>{renderBtn()}</>;
};

export default Login;