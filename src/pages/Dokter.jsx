import React, { useState } from "react";
import Header from "../layout/Header";
import { Switch, Route, NavLink, useRouteMatch } from "react-router-dom";
import "./Dokter.css";
import { Container, Row, Col } from "reactstrap";
import DoctorLeftSide from "../components/DoctorLeftSide";
import DoctorApproval from "../components/DoctorApproval";
import BookFromPatient from "../components/BookFromPatient";
import EditDoctorRight from "../components/EditDoctorRight";
import { checkIsDoctor } from "../Helper";

const Dokter = () => {
  const [isLoading, setIsLoading] = useState(false);
  let { path, url } = useRouteMatch();
  console.log(path);

  const [editProfile, setEditProfile] = useState(false);

  const handleEditProfile = () => {
    setEditProfile(!editProfile);
  };

  //ada komponen book from patient default
  //ada komponen approval clicked

  if (!checkIsDoctor()) {
    return (
      <>
        <Header />
        <div className="dokter-background">
          <br />
          <br />
          <br />
          <br />
          <br />
          <Container fluid={true}>
            <Row>
              <Col sm="4">
                <DoctorLeftSide handleEditProfile={handleEditProfile} />
              </Col>
              <Col sm="8">
                <div className="dokter-right-box">
                  {editProfile && (
                    <Switch>
                      <Route path={`${path}/edit-profile`}>
                        <EditDoctorRight />
                      </Route>
                    </Switch>
                  )}
                  {!editProfile && (
                    <>
                      <Row>
                        <Row className="mt-4 ml-4">
                          <NavLink
                            to={path}
                            exact={true}
                            className="nav-link inactive-nav mr-4"
                            activeClassName="active-nav"
                          >
                            Approval
                          </NavLink>
                          <NavLink
                            to={`${path}/book-from-patient`}
                            exact={true}
                            className="nav-link inactive-nav ml-4"
                            activeClassName="active-nav"
                          >
                            Book From Patient
                          </NavLink>
                        </Row>
                      </Row>
                      <hr />
                    </>
                  )}

                  <Row className="mb-4">
                    <Switch>
                      <Route exact path={`${path}`}>
                        <DoctorApproval />
                      </Route>
                    </Switch>
                    <Switch>
                      <Route exact path={`${path}/book-from-patient`}>
                        <BookFromPatient />
                      </Route>
                    </Switch>
                  </Row>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </>
    );
  } else {
    return (
      <>
        <Header />
        <br />
        <br />
        <br />
        <br />
        <br />
        <h1>only for doctors</h1>
      </>
    );
  }
};

export default Dokter;
